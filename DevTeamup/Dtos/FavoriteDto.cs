﻿namespace DevTeamup.Dtos
{
    public class FavoriteDto
    {
        public string FavoredUserId { get; set; }
    }
}