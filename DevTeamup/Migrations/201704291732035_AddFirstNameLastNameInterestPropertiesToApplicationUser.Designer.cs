// <auto-generated />
namespace DevTeamup.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddFirstNameLastNameInterestPropertiesToApplicationUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddFirstNameLastNameInterestPropertiesToApplicationUser));
        
        string IMigrationMetadata.Id
        {
            get { return "201704291732035_AddFirstNameLastNameInterestPropertiesToApplicationUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
