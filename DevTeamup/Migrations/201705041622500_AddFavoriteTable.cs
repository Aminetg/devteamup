namespace DevTeamup.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFavoriteTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Favorites",
                c => new
                    {
                        FavoringUserId = c.String(nullable: false, maxLength: 128),
                        FavoredUserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.FavoringUserId, t.FavoredUserId })
                .ForeignKey("dbo.AspNetUsers", t => t.FavoringUserId)
                .ForeignKey("dbo.AspNetUsers", t => t.FavoredUserId)
                .Index(t => t.FavoringUserId)
                .Index(t => t.FavoredUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Favorites", "FavoredUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Favorites", "FavoringUserId", "dbo.AspNetUsers");
            DropIndex("dbo.Favorites", new[] { "FavoredUserId" });
            DropIndex("dbo.Favorites", new[] { "FavoringUserId" });
            DropTable("dbo.Favorites");
        }
    }
}
