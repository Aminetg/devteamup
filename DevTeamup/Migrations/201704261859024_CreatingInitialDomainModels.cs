namespace DevTeamup.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatingInitialDomainModels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DevelopmentLanguages",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DevelopmentTypes",
                c => new
                    {
                        Id = c.Byte(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Teamups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Address = c.String(nullable: false, maxLength: 255),
                        DateTime = c.DateTime(nullable: false),
                        Description = c.String(nullable: false),
                        DevelopmentLanguage_Id = c.Byte(nullable: false),
                        DevelopmentType_Id = c.Byte(nullable: false),
                        Programmer_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DevelopmentLanguages", t => t.DevelopmentLanguage_Id, cascadeDelete: true)
                .ForeignKey("dbo.DevelopmentTypes", t => t.DevelopmentType_Id, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.Programmer_Id, cascadeDelete: true)
                .Index(t => t.DevelopmentLanguage_Id)
                .Index(t => t.DevelopmentType_Id)
                .Index(t => t.Programmer_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teamups", "Programmer_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Teamups", "DevelopmentType_Id", "dbo.DevelopmentTypes");
            DropForeignKey("dbo.Teamups", "DevelopmentLanguage_Id", "dbo.DevelopmentLanguages");
            DropIndex("dbo.Teamups", new[] { "Programmer_Id" });
            DropIndex("dbo.Teamups", new[] { "DevelopmentType_Id" });
            DropIndex("dbo.Teamups", new[] { "DevelopmentLanguage_Id" });
            DropTable("dbo.Teamups");
            DropTable("dbo.DevelopmentTypes");
            DropTable("dbo.DevelopmentLanguages");
        }
    }
}
