namespace DevTeamup.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForiegnKeyPropertiesToTeamup : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Teamups", name: "DevelopmentLanguage_Id", newName: "DevelopmentLanguageId");
            RenameColumn(table: "dbo.Teamups", name: "DevelopmentType_Id", newName: "DevelopmentTypeId");
            RenameColumn(table: "dbo.Teamups", name: "Programmer_Id", newName: "ProgrammerId");
            RenameIndex(table: "dbo.Teamups", name: "IX_Programmer_Id", newName: "IX_ProgrammerId");
            RenameIndex(table: "dbo.Teamups", name: "IX_DevelopmentType_Id", newName: "IX_DevelopmentTypeId");
            RenameIndex(table: "dbo.Teamups", name: "IX_DevelopmentLanguage_Id", newName: "IX_DevelopmentLanguageId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Teamups", name: "IX_DevelopmentLanguageId", newName: "IX_DevelopmentLanguage_Id");
            RenameIndex(table: "dbo.Teamups", name: "IX_DevelopmentTypeId", newName: "IX_DevelopmentType_Id");
            RenameIndex(table: "dbo.Teamups", name: "IX_ProgrammerId", newName: "IX_Programmer_Id");
            RenameColumn(table: "dbo.Teamups", name: "ProgrammerId", newName: "Programmer_Id");
            RenameColumn(table: "dbo.Teamups", name: "DevelopmentTypeId", newName: "DevelopmentType_Id");
            RenameColumn(table: "dbo.Teamups", name: "DevelopmentLanguageId", newName: "DevelopmentLanguage_Id");
        }
    }
}
