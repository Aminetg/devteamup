﻿using DevTeamup.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DevTeamup.ViewModels;

namespace DevTeamup.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController()
        {
            _context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var futureTeamups = _context.Teamups
                .Include(t => t.Programmer)
                .Include(t => t.DevelopmentLanguage)
                .Include(t => t.DevelopmentType)
                .Where(t => t.DateTime > DateTime.Now)
                .OrderBy(t => t.DateTime)
                .ToList();

            var viewModel = new HomeViewModel
            {
                FutureTeamups = futureTeamups,
                IsAuthenticated = User.Identity.IsAuthenticated
            };

            return View(viewModel);
        }

    }
}