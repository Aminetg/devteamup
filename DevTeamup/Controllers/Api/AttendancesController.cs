﻿using System.Linq;
using DevTeamup.Models;
using Microsoft.AspNet.Identity;
using System.Web.Http;
using DevTeamup.Dtos;

namespace DevTeamup.Controllers.Api
{
    [Authorize] 
    public class AttendancesController : ApiController
    {
        private readonly ApplicationDbContext _context;

        public AttendancesController()
        {
            _context = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult Join(AttendanceDto dto)
        {
            var userId = User.Identity.GetUserId();

            if (_context.Attendances.Any(a => a.AttendeeId == userId && a.TeamupId == dto.TeamupId))
                return BadRequest("It aready exists");
            
            var attendance = new Attendance
            {
                TeamupId = dto.TeamupId,
                AttendeeId = userId
            };

            _context.Attendances.Add(attendance);
            _context.SaveChanges();

            return Ok();
        }
    }
}
