﻿using System.Collections.Generic;
using DevTeamup.Models;

namespace DevTeamup.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Teamup> FutureTeamups { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}