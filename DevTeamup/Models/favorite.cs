﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DevTeamup.Models
{
    public class Favorite
    {
        [Key]
        [Column(Order = 1)]
        public string FavoringUserId { get; set; }

        [Key]
        [Column(Order = 2)]
        public string FavoredUserId { get; set; }

        public ApplicationUser FavoringUser { get; set; }

        public ApplicationUser FavoredUser { get; set; }
        
    }
}