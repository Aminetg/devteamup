using System.Data.Entity;
using DevTeamup.Migrations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevTeamup.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Teamup> Teamups { get; set; }
        public DbSet<DevelopmentType> DevelopmentTypes { get; set; }
        public DbSet<DevelopmentLanguage> DevelopmentLanguages { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<Favorite> Favorites { get; set; }


        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());

            modelBuilder.Entity<Attendance>()
                .HasRequired(a => a.Teamup)
                .WithMany()
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(u => u.FavoringUsers)
                .WithRequired(f => f.FavoredUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                .HasMany(u => u.FavoredUsers)
                .WithRequired(f => f.FavoringUser)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }
    }
}